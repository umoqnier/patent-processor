from procesador_patentes_otorgadas import get_xml_file, create_folders

HEADER = ["Número de solicitud", "Fecha de presentación", "Solicitante(s)", "Inventor(es)", "Agente",
          "Prioridad (es)", "Clasificación CIP", "Título", "Resumen", "Número de solicitud internacional",
          "Fecha de presentación internacional", "Número de publicación internacional",
          "Fecha de publicación internacional"]


def base_file_generator(year, month):
    if month > 9:
        base = "PA_SO_" + year + '_' + str(month) + "_001.xml"
    else:
        base = "PA_SO_" + year + '_0' + str(month) + "_001.xml"
    return base


def field_formatter(root):
    data = ""
    total = 0
    fields_iter = iter(HEADER)
    for patents in root:
        section = patents.attrib['nombre']
        print(">>>>>>> Procesando", section, "<<<<<<<<<<")
        data += section + "\n"
        for i, patent in enumerate(patents):
            xml_fields = patent.findall("campo")
            xmls_fields_iter = iter(xml_fields)
            current_field = fields_iter.__next__()
            while True:
                try:
                    field = xmls_fields_iter.__next__()
                except StopIteration:
                    print("\t\t[WARNING] No se encontro", current_field)
                    data += 'NO DISPONIBLE|'
                    if current_field == "Fecha de publicación internacional":
                        fields_iter = iter(HEADER)
                        break
                    current_field = fields_iter.__next__()
                    print("\t\t****Search:", current_field)
                    xmls_fields_iter = iter(xml_fields)
                    continue
                key = field.find("clave").text
                if key == current_field:
                    value = field.find("valor").text
                    if value:
                        data += value + '|'
                    else:
                        data += "NO DISPONIBLE|"
                    if key == "Fecha de publicación internacional":
                        fields_iter = iter(HEADER)
                        break
                    current_field = fields_iter.__next__()
                    xmls_fields_iter = iter(xml_fields)
            data += '\n'
        print(">> Patentes procesadas:", len(patents))
        total += len(patents)
    print("*/////* Total procesadas", total)
    return data


def main():
    year = "2018"
    create_folders(year)
    for month in range(1, 13):
        print("Patentes solicitadas en", year)
        base = base_file_generator(year, month)
        print("Procesando >>", base)
        try:
            root = get_xml_file(base)
        except FileNotFoundError:
            print("[WARNING] No se encontro el archivo", base)
            continue
        data = field_formatter(root)
        out_name = base[:-4] + ".csv"
        with open("csv/" + year + "/" + out_name, "w") as f_out:
            f_out.write('|'.join(HEADER) + '\n')  # Header
            f_out.write(data)
        print("***Terminado >>", out_name)


if __name__ == '__main__':
    main()
