# patent-processor
Procesador de patentes facilitadas por la [página](https://siga.impi.gob.mx/newSIGA/content/common/descargaEjemplares.jsf) Instituto Mexicano de la Propiedad Intelectual. El formato requerido es `XML` y la salida es `CSV`.
Las patentes deberán estar una carpeta llamada `patentes_xml`.

## Tipos de patentes
* Otorgadas
* Solicitadas

## Campos recuperados por patente
### Otorgadas
* Número de concesión
* Tipo de documento
* Número de solicitud
* Fecha de presentación
* Fecha de concesión
* Clasificación CIP
* Título
* Resumen
* Inventor(es)
* Titular

### Solicitadas
* Número de solicitud
* Fecha de presentación
* Solicitante(s)
* Inventor(es)
* Agente
* Prioridad (es)
* Clasificación CIP
* Título
* Resumen
* Número de solicitud internacional
* Fecha de presentación internacional
* Número de publicación internacional
* Fecha de publicación internacional