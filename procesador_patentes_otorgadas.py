import xml.etree.ElementTree as ET
import os


def get_xml_file(name):
    tree = ET.parse("patentes_xml/"+name)
    return tree.getroot()


def search_patents_in_tree(root):
    for child in root:
        t = child.attrib
        if t['nombre'] == "Patentes":
            patents = child
            return patents


def field_formatter(root):
    data = ""
    header = ["Número de concesión", "Tipo de documento", "Número de solicitud", "Fecha de presentación", "Fecha de concesión",
                "Clasificación CIP", "Título", "Resumen", "Inventor(es)", "Titular"]
    fields_iter = iter(header)
    patents = search_patents_in_tree(root)
    for patent in patents:
        xml_fields = patent.findall("campo")
        xml_fields_iter = iter(xml_fields)
        current_field = fields_iter.__next__()
        while True:
            field = xml_fields_iter.__next__()
            key = field.find("clave").text
            if key == current_field:
                value = field.find("valor").text
                data += value + '|'
                if key == "Titular":
                    fields_iter = iter(header)
                    break
                current_field = fields_iter.__next__()
                xml_fields_iter = iter(xml_fields)
        data += '\n'
    print(">> Patentes procesadas:", len(patents))
    return data


def create_folders(year):
    print("*** Creando carpetas para csv de salida...")
    try:
        os.mkdir("csv")
    except FileExistsError:
        pass

    try:
        os.mkdir("csv/" + year)
    except FileExistsError:
        pass

    print("*** Terminado")
    return 0


def base_file_generator(year, month):
    if month > 9:
        base = "PA_RE_" + year + '_' + str(month) + "_001.xml"
    else:
        base = "PA_RE_" + year + '_0' + str(month) + "_001.xml"
    return base


def main():
    year = "2018"
    create_folders(year)
    for month in range(1, 13):
        print("Procesando patentes orotgadas en", year)
        base = base_file_generator(year, month)
        print("Procesando >>", base)
        root = get_xml_file(base)
        data = field_formatter(root)
        out_name = base[:-4] + ".csv"
        with open("csv/" + year + "/" + out_name, "w") as f_out:
            f_out.write("Número de concesión|Tipo de documento|Número de solicitud|Fecha de presentación|Fecha de"
                        " concesión|Clasificación CIP|Título|Resumen|Inventor(es)|Titular\n")  # Header
            f_out.write(data)
        print("***Terminado >>", out_name)


if __name__ == '__main__':
    main()
